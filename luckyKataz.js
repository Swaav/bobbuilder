//for each function is just a for loop at the end
//of the day nothing to it, basically just provides
//you a basic for loop so u dont need to do it yourself
//feel me? oh yea and dont mind the language 
//i was in a very baaaad place
//The forEach() method executes a provided function once 
//for each array element.
//so basically i start off creating my function here, which takes two
//arguements, first one being your collection(array in question)
//and the second one being onElementIterationCallback just meaning that 
//i am iterating through each piece of garbage in the array. I first make a 
//for loop that lets i into the collection, and inside of that banging body
//i say if(collection.hasOwnProperty(i)) meaning that IF the collection or array
//has any type of substanence of (i) or better put [i] for my understanding, which it
//does because we just let i into the collection; then we will open up that banging body,
//and say onElementIterationCallback(collection[i], i); meaning that im taking the collection
//and applying which ever function which is the onElementIterationCallback; to each 
//and every single element in the array, and im guessing the extra i at the end is...
//actually i dont know what that last i is for

function forEachBitch(collection, onElementIterationCallback) {
    for (let i in collection) {
        if (collection.hasOwnProperty(i)) {
            onElementIterationCallback(collection[i], i, collection);
        }
    }
}

sumofAges = 0;
people = [
    { name: "Angel", age: 23 },
    { name: "Butterscotch", age: 34 },
    { name: "nympho", age: 19 }
]

forEachBitch(people, function (person, currentOffset) {
    console.log("(" + currentOffset + ") iterating on " +
        person.name + ", age: " + person.age);
    sumofAges += person.age;
});
//expected outcome

//  (0) iterating on Angel, age: 23
//  (1) iterating on Butterscotch, age: 34
//  (2) iterating on nympho, age: 19

console.log(sumofAges);
//a map function in JavaScript is the ability to take in an arrray and 
// return the array as modified through user contingencies

// /var test = [1,2,3,4] ===> return test expected as test = [2,4,6,8]

test = [1, 2, 3, 4];
// newArray SHOULD EQUAL [2,4,6,8];

//regular function expresion

console.log('Before ' + test)
function myMAPbish(array, operation) {
    let newArray = [];
    for (i = 0; i < array.length; i++) {
        newArray.push((operation(array[i], i, newArray)))
    }
    console.log('After ' + newArray)
}
myMAPbish(test, function (element) {
    return element * 2;
})


// immediately invoked function expression, same forEach
//situation
let newArray = [];
console.log('Before ' + test);
(function (array, operation) {
    for (i = 0; i < array.length; i++) {
        newArray.push(operation(array[i], i, newArray));

    }
    console.log('After ' + newArray);
})(test, function (index) {
    return index * 2;
});

//The some() method tests whether at least one element in the array 
//passes the test implemented by the provided function.
//Note: This method returns false for any condition put on an empty array.
//so i start off creating a function which takes the array and my operation,
//i initialize a new array and i initialize a count
//after words pretty regular for loop with my operation manipulating 
//each index of the array, then pushing that array index and my operation 
//to a completely new array, then if the index of the new array matches anything 
//in the previous array it adds to the count and if count is greater then 0 we have true
//if else its false

function someME(array, operation) {
    newArray = []
    count = 0

    for (let i = 0; i < array.length; i++) {
        operation(array[i])
        newArray.push(operation(array[i], i, newArray))

        if (newArray[i] === true) {
            count += 1
        }
    }
    if (count > 0) {
        return "true"
    } else {
        return "false"
    }

}

let test2 = [1, 3, 5, 7, 9]

let even = someME(test2, function (element) {
    return element % 2 === 0
})
//expected out come of test--false 
console.log(even)



//ok so basically what im doing here is looping through the array
//and im using this prototype method or whatever.
//array.prototype.findme literally means that i am using an array
//and creating a prototype function for that array; even tho prototype is global
//the actually operation is what im using to find whatever it is i am doing,
//for instance if i wanted to find if the first index is less then 200, i would
//put that into the operation, expected that i should get back the first number that
//less then 200, or i can say im looking for an EXACT number, in the first spot
//i start of looping through whatever array per say, and then i say 
//if the operation(this[i]) meaning that if the operation pertains to anything
//in THIS, THIS being the array; then i will return that value. well in this case, 
//i will return the exact value of the first spot(or element) in the array but only if it 
//fits my operation!

Array.prototype.findHim = function (operation) {
    for (let i = 0; i < this.length; i++) {
        if (operation(this[i], i, newArray)) {
            return this[i]
        }
    }
}

test3 = [13, 12, 8, 130, 44];

console.log(test3.findHim(function (i) {
    return i == 131
}));
// expected output: undefined

//FINDING THAT INDEX BOYYY
//this is essentially the same thing as the above function cept
//we are returning the index of the first element in the array that
//satisfies the operation returning you an array
test4 = [5, 12, 8, 130, 44];


Array.prototype.findHer = function (operation) {
    for (let i = 0; i < this.length; i++) {
        if (operation(this[i], i, newArray)) {
            return i
        }
    }
}

console.log(test4.findHer(function (i) {
    return i > 13

}));
//expected result 3 meaning there are 3 elements
//greater then 13, returning the array basically, the before
//one represents the value of an element inside the array
//while this one returns information about the whole array,
//u basically get the whole array in this one, well the whole
//array based on the operation(function) im creating

console.log(test4.findHer(function (i) {
    return i = 5
}));
// expected result 0 meaning 1


//the every () method tests whether all elements in the array pass 
//the test implemented by the provided function. so i basically
//start off with this awesum prototype jazz. array.prototype.everyEye=function(operation)
//means i have an array prototype which im putting through a function
//and a operation, i let output cuz im just initializing, 
//and start off with basic for loop to get awesome with every spot in the array,
//this being the array, and then i say if the operation pertaining to this[i] is true,
//then the output is true, if else, the output will be false, afterwards i return 
//the output, and then outside the if else statements but inside the for loop
//i return the output again if it happens to be false otherwise it would just be undefined.  

Array.prototype.everyEye= function (operation) {
    let output;
    for (let i = 0; i < this.length; i++) {
        if (operation(this[i], i, newArray) === true) {
            output = true
        } else {
            output = false
            return output
        }
    }
    return output
}

test5 = [2,6,9,3,0,2,51,34]

console.log(test5.everyEye(function(a){
    return a < 60
}));

console.log(test5.everyEye(function(i){
    return i > 0
}))
//HEY I DID THIS ONE ON MY OWN YOU MANGY ALIENS
//the filter() method creates a new array with all elements
//that pass the test implemented by the provided function
Array.prototype.filterSauce = function (operation) {
    const buttcheeks = []
    for (let i = 0; i < this.length; i++) {
        if (operation(this[i], i, newArray)===true) {
            buttcheeks.push(this[i])
        
        }
        
    }
    return buttcheeks
}
let words = ['spray', 'limit', 'elite', 'exuberant','destruction','present'];

const result = words.filterSauce(word => word.length > 6)

console.log(result)